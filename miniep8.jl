using Test

function testes()
	@test(compareByValue("2♠", "A♠"))
	@test(!compareByValue("K♥", "10♥"))
	@test(!compareByValue("10♠", "10♥"))
	@test(compareByValueAndSuit("2♠", "A♠"))
	@test(!compareByValueAndSuit("K♥", "10♥"))
	@test(compareByValueAndSuit("10♠", "10♥"))
	@test(compareByValueAndSuit("A♠", "2♥"))
	@test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
	println("fim dos testes")
end

function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1])
				troca(v, j, j - 1)
			else
				break
			end
			j = j - 1
		end
	end
	return v
end

function compareByValue(x, y)
	x = auxiliarValor(x)
	y = auxiliarValor(y)
	if x < y
		return true
	else
		return false
	end
end

function compareByValueAndSuit(x, y)
	a = auxiliarNaipe(x)
	b = auxiliarNaipe(y)
	if a < b
                return true
        elseif a == b
		if compareByValue(x, y)
			return true
		else
			return false
		end
	else
                return false
        end
end

function auxiliarValor(x)
	if x[1] == '2'
		return 2
	elseif x[1] == '3'
		return 3
	elseif x[1] == '4'
                return 4
	elseif x[1] == '5'
                return 5
	elseif x[1] == '6'
                return 6
	elseif x[1] == '7'
                return 7
	elseif x[1] == '8'
                return 8
	elseif x[1] == '9'
                return 9
	elseif x[1] == '1'
                return 10
	elseif x[1] == 'J'
                return 11
	elseif x[1] == 'Q'
                return 12
	elseif x[1] == 'K'
                return 13
	elseif x[1] == 'A'
                return 14
	end
end


function auxiliarNaipe(x)
	tam = length(x)
	if x[tam] == '♦'
		return 1
	elseif x[tam] == '♠'
		return 2
	elseif x[tam] == '♥'
		return 3
	elseif x[tam] == '♣'
		return 4
	end
end

testes()
